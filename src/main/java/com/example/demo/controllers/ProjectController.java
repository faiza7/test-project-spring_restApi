package com.example.demo.controllers;

import com.example.demo.models.Project;
import com.example.demo.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by Faza on 7/31/2017.
 */
@RestController
@RequestMapping("/project")
public class ProjectController {

    final static Logger logger = Logger.getLogger(ProjectController.class);
    @Autowired
    private ProjectService projectService;

    @RequestMapping(method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Project>> getAllProject(){
        List<Project> projects = projectService.getAll();
        return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);

    }
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> addProject(@RequestBody Project project){
        projectService.save(project);
        logger.debug("Added:: " + project);
        return new ResponseEntity<Project>(project,HttpStatus.CREATED);
    }
}
