package com.example.demo.controllers;


import com.example.demo.models.User;
import com.example.demo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Faza on 8/30/2017.
 */
@RestController
@RequestMapping
public class LoginController {
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value="/login",method= RequestMethod.POST)
    public ResponseEntity<User> login(@RequestBody User user){

        String username = user.getUsername();
        String password = user.getPassword();
        User user1 = userService.loginUser(username,password);
        logger.info("Logged in successfully:: " + user1.getId() );
                if(user1.getId() == null){
            logger.debug("Log in error!" );
        }

        return new ResponseEntity<User>(user1, HttpStatus.OK);
    }



}
