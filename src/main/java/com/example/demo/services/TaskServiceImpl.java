package com.example.demo.services;
import com.example.demo.repositories.TaskRepository;
import com.example.demo.models.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.TransactionScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Faza on 7/31/2017.
 */
@Service
@TransactionScoped
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task save(Task entity){return taskRepository.save(entity);}

    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }
    @Override
    public void delete(Serializable id) {
        taskRepository.delete((Long) id);
    }
    @Override
    public Task getById(Serializable id) {
        return taskRepository.findOne((Long) id);
    }
}