package com.example.demo.services;

import com.example.demo.models.User;
import com.example.demo.repositories.RoleRepository;
import com.example.demo.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.TransactionScoped;
import java.util.HashSet;
import java.util.List;


/**
 * Created by Faza on 8/30/2017.
 */
@Service
@TransactionScoped
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;


    @Override
    public void save(User user){
        user.setPassword(user.getPassword());
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);

    }
    @Override
    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }
    @Override
    public User findByPassword(String password){
        return userRepository.findByPassword(password);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User loginUser(String username, String password){

             User usern1 = userRepository.findByUsernameAndPassword(username,password);
//             User usern2 = userRepository.findByPassword(password);
//         Boolean y = usern1.equals(usern2);
//            return y;

        return usern1;
    }

}
