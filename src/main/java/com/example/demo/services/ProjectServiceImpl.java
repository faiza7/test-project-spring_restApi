package com.example.demo.services;
import com.example.demo.models.Project;
import com.example.demo.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.TransactionScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Faza on 7/31/2017.
 */
@Service
@TransactionScoped
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project save(Project entity){return projectRepository.save(entity);}

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public void delete(Serializable id) {
       projectRepository.delete((Long) id);
    }
    @Override
    public Project getById(Serializable id) {
        return projectRepository.findOne((Long) id);
    }
}
