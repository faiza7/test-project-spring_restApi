package com.example.demo.services;

import com.example.demo.models.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Faza on 8/30/2017.
 */
@Component
public interface UserService {
    User findByUsername(String username);
    User findByPassword(String password);
    List<User> getAll();
    void save(User user);
   User loginUser(String username, String password);

}
