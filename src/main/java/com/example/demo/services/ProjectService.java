package com.example.demo.services;

import com.example.demo.models.Project;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Faza on 7/31/2017.
 */
@Component
public interface ProjectService {
    Project save(Project project);
    List<Project> getAll();
    Project getById(Serializable id);
    void delete(Serializable id);

}
