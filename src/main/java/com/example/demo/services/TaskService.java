package com.example.demo.services;

import com.example.demo.models.Task;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Faza on 7/31/2017.
 */
@Component
public interface TaskService {
   Task save(Task entity);
    List<Task> getAll();
    Task getById(Serializable id);
    void delete(Serializable id);
}
