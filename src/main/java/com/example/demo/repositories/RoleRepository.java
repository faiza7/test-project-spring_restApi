package com.example.demo.repositories;

import com.example.demo.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Faza on 8/30/2017.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
