package com.example.demo.repositories;

import com.example.demo.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Faza on 7/31/2017.
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}

