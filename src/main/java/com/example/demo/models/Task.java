package com.example.demo.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Faza on 7/31/2017.
 */

@Entity
@Table(name = "Task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column
    @GeneratedValue
    private Long task_id;

    @Column(name="description")
    private String description;

    @ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER )
    @JoinColumn(name = "project")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProjectId(Project project) {
        this.project = project;
    }



    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
